Список замечаний к странице (крестиком отмечены исправленные):
Ссылка на исправленную сраницу - https://codepen.io/YuliaYulia/full/MWKPvPb
Скриншот также в этом репозитории

	1. 
[x]Логотип и не по центру и не сбоку - стоит выровнять относительно элементов в главном блоке с контентом и ссылки Home, так чтобы начало буквы "В" в логотипе совпадало с началом текста.
	2. 
[x]Логотип должен быть ссылкой на главную страницу, а значит должен быть cursor = pointer
	3. 
[x]Иконка пользователя тоже может быть кликабельным элементом, по нажатию на который открывается например меню (как в gmail) - добавить pointer cursor на наведени
	4. 
[x]В объекте "Хлебные крошки" страница Home должна быть ссылкой, значит должен быть pointer = cursor. 
Вообще, так как структура сайта небольшая - на данной странице всего два уровня вложенности, то "Хлебные крошки" можно и не использовать.
	5. 
[x]Цвет линки Home имеет недостаточную контрастность (должно быть больше 4,5)
	6. 
[x]Заголовок должен быть выровнен по левому краю относительно логотипа, меню 
	7. 
[x]Межстрочное расстояние многих элементов (кроме значений календаря) не равно оптимальному (должно быть 1.2)

КНОПКИ
	8. 
[x]Кнопки Export CSV Import CSV - вторичные, они предлагают альтернативное действие кнопки Add Request, для данных кнопок рекомендуется использовать контурные кнопки - прозрачный цвет, текст и контур одного цвета
	9. 
[x]Для вторичных кнопок лучше сделать прозрачный фон и контур, при наведении - выделение цветом, при фокусировании - чуть темнее, при активации - еще чуть темнее
	10. 
[x]Немного увеличить растояние между буквами в тексте кнопок
	11. 
[x]У кнопок Add Request - нет радиуса скругления.  Округленные края усиливают восприятие информации и притягивают взгляд к центру элемента, поэтому нужно сделать кнопку, так это основное действие. В тоже время нельзя делать все кнопки рядом скругленными одинаково, поэтому Export CSV иImport CSV скруглим совсем чуть-чуть .
	12. 
[x]При наведении на Export CSV иImport CSV кнопки сделать cursor = pointer
	13. 
[x]При наведении на Export CSV кнопка не выделяется цветом, непонятно, что на неё навели
	14. 
[x]Я всегда встречала в программах и модальных окнах, что сначала идет выше/левее кнопка Import, а после неё кнопка Export, так как было сказано, что лучше делать так как пользовательский глаз уже привык в других интерфейсах (пример с текстом капсом и не капсом в лекции), то лучше поменять кнопки местами
	15. 
[x]Текст кнопки Add Request сделать другим цветом, а не серым - так лучше читается текст,  и исходное значение контрастности 1.81 (должно быть больше 4,5). Можно было бы заменить текст кнопки на плюсик, если бы рядом не было других кнопок касающихся CSV border: 1px solid transparent; background: #bec3bfbd;
	16. 
[x]Cделать кнопку  Add Request объемной (добавить тень), а Export CSV Import CSV - плоскими, так они не будут отвлекать внимание от главной кнопки.
	17. 
[x]При нажатии на кнопки появляется рамка несоответсвующая границе самой кнопки - убрать рамку
	18. 
[x]Cделать отклик в цвете при наведении на кнопку Add Request, более яркий, это делает кнопку визуально активной.
	19. 
[x]Добавить отступы между кнопками
	20. 
[x]Выровнять кнопки по правому краю относительно большого контейнера с карточками и фото пользователя

	21. 
[x]Лейблы полей ввода занимают много места - лучше сделать информацию о вводе как placeholder
	22. 
[x]Выровнять поля ввода по левой стороне относительно карточек

КАРТОЧКИ
	23. 
[x]Карточки имеют разную ширину из-за этого нет четких колонок - использовать в контейнере вместо flex, grid сделать колонки одной ширины в зависимости от ширины экрана
	24. 
[x]Увеличить отступы между карточками и внутренние отступы контейнера, в котором карточки
	25. 
[x]Рядом с иконкой человечка должно быть описание того, что эта иконка значит - нужно скомбинировать иконку, её значение и описание в читабельную фразу.  
	26. 
[x]Карточка содержит несколько иконок календаря, Это может запутать пользователя, причем синяя иконка не несет смысловой нагрузки - удалить
	27. 
[x]Текст карточки залазит под контейнер с датой
	28. 
[x]Выровнять начало текста с иконкой даты и датой под начало синего квадрата с началом текста даты и иконку календаря. Также текст с количеством позиций и текстом в метке должен быть на одном уровне - поставить одинаковые паддинги.
	29. 
[x]Изменить цвета меток статусов - более яркие цвета для статусов, которые требуют действия и с цветом текста под цвет фона (как было в видео) -  
Cancelled - серый, эти карточки уже не актуальны и не должны привлекать внимания
Completed - так как действия уже сделаны, привлекать внимание не должно, но также нужно передать информацию об успехе дествия, поэтому прозрачный не яркий-зеленый.
Accepted - принято и требует действия незамедлительно, зеленый самый яркий,
Pending  - требует чего-то действия - желтый яркий
	30. 
[x]Метки со статусом должен иметь большие padding
	31. 
[x]У цветного блока с датой очень жирная тень, сильно выделяется 
	32. 
[x]Время работы должно быть меньшим размером шрифта, и иметь большие отступы от самой главной информации - название специалиста который требуется, потому что это более глобальная информация - сначала человек будет просматривать позиции под которые подходит по профессии, по названию компании, а потом выбирать те, под которые подходит его личное расписание
	33. 
[x]Название специальности и степень - тоже важная информация, стоит разместить по центру и сделать чуть больше шрифт. Показывать всю информацию в этих блоках.
	34. 
[x]Дата создания карточки и автор маловажная информация - можно сделать маленьким шрифтом
	35. 
[x]По наведению на карточку должен быть указатель - cursor = pointer, потому что пользователь может нажать на саму карточку и развернуть её в новом окне

	36. 
[x]Текст в footer прилепился верхней границей к предыдущему контейнеру - установить внешние отступы footer

